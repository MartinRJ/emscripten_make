#!/bin/bash
if [[ -z $HAM_HOME ]]; then
    echo "E/HAM_HOME not set !"
    exit 1
fi
. $HAM_HOME/bin/ham-bash-lib.sh
export HAM_CMD="$HAM_HOME/bin/$HAM_BIN_LOA/ham"

# the help
usage() {
    echo "usage: ham [ options ] targets..."
    echo "version:" `${HAM_CMD} -v`
    echo ""
    echo "ham launch script:"
    echo "-h             This help message"
    echo "-B target      Run ham once before with the specified target"
    echo "-T toolset     Add a toolset to import"
    echo "               (can be specified multiple times)"
    echo "-D directory   Add a toolkit to build - the toolkit should be in $WORK"
    echo "               (can be specified multiple times)"
    echo "--             Following dash '-' parameters will be passed to ham"
    echo ""
    echo "ham process:"
    "${HAM_CMD}" -? | grep "^[- ]"
    echo ""
    echo "example:"
    echo "  # Build all in the release build"
    echo "  ham BUILD=ra all"
    echo "  # Build all in the current toolkit, displaying all the commands"
    echo "  ham -T defaultj -- -dx all"
    echo "  # Build all in the niLang & niSDK toolkits"
    echo "  ham -T defaultj -D niLang -D niSDK all"
    echo ""

    if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
        # sourced...
        return 1
    else
        # regular call
        exit 1
    fi
}

# Check that there is at least one argument...
if [ $# = 0 ]; then
    echo "E/No argument specified"
    echo ""
    usage
fi

# process the arguments
_ARG_DIRECTORIES=()
while getopts :T:D:B:v OPT
do
    case "$OPT" in
        h)
            usage
            ;;
        \?)
            usage
            ;;
        v)
            "$HAM_CMD" -v
            if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
                return 0
            else
                exit 0
            fi
            ;;
        T)
            _ARG_TOOLSET="$_ARG_TOOLSET $OPTARG"
            # echo "_ARG_TOOLSET:" $_ARG_TOOLSET
            ;;
        D)
            if  [[ "$OPTARG" == ?:/* ]] || [[ "$OPTARG" == /* ]] || [[ "$OPTARG" == .* ]] ;
            then
                # Explicit path
                _ARG_DIRECTORY="$OPTARG/sources/"
                if [[ ! -e "$_ARG_DIRECTORY" ]]; then
                    _ARG_DIRECTORY="$OPTARG"
                    if [[ ! -e "$_ARG_DIRECTORY" ]]; then
                        echo "E/Can't find directory '$_ARG_DIRECTORY'"
                        usage
                    fi
                fi
            else
                # Work App path
                _ARG_DIRECTORY="$WORK/$OPTARG/sources/"
            fi
            if [[ ! -e "$_ARG_DIRECTORY" ]]; then
                echo "E/Can't find directory '$_ARG_DIRECTORY'"
                usage
            fi
            _ARG_DIRECTORIES+=($_ARG_DIRECTORY)
            ;;
        B)
            _ARG_BUILD_BEFORE=$OPTARG
            ;;
    esac
done
shift $((OPTIND-1))

if [[ "$_ARG_TOOLSET" != "" ]]; then
    . $HAM_HOME/bin/ham-bash-setenv.sh
    . ham-toolset $_ARG_TOOLSET
    errcheck $? ham "Can't setup the '$_ARG_TOOLSET' toolset."
fi

if [[ -z $HAM_TOOLSET ]]; then
    echo "E/No toolset set, you can set one with the -T flag or '. ham-toolset' ..."
    usage
fi

hamIn() {
    export CWD=`pwd`
    export DIR="$1"
    shift
    if [[ "$CWD" != "$DIR" ]]; then
        pushd $DIR
    fi
    if [[ "$_ARG_BUILD_BEFORE" != "" ]]; then
        "$HAM_CMD" $_ARG_BUILD_BEFORE
    fi
    echo "$HAM_CMD" $*
    "$HAM_CMD" $*
    errcheck $? ham "Build in $DIR failed."
    if [[ "$CWD" != "$DIR" ]]; then
        popd
    fi
}

if [[ "$_ARG_DIRECTORY" != "" ]]; then
    for dir in "${_ARG_DIRECTORIES[@]}"; do
        hamIn $dir $*
    done
else
    # find the _build.ham file
    export BUILD_HAM_DIR=`ham-search-build-ham.sh`
    if [[ -z $BUILD_HAM_DIR ]]; then
        echo "E/Can't find _build.ham !"
        if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
        # sourced...
            return 1
        else
        # regular call
            exit 1
        fi
    fi
    hamIn $BUILD_HAM_DIR $*
fi
