/* Keep JAMVERSYM in sync with VERSION. */
/* It can be accessed as $(JAMVERSION) in the Jamfile. */

#define VERSION "6 (" __DATE__ ")"
#define JAMVERSYM "JAMVERSION=6"
