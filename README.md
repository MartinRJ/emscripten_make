Ham
===

Ham is a language agnostic 'make' replacement.

It is a derivative of "FT-Jam" which is itself a derivative of the Jam build
tool, based and 100% compatible with Jam 2.5.

- The FT-Jam homepage: http://www.freetype.org/jam/
- The original Jam homepage: http://www.perforce.com/jam/jam.html

Ham is MIT licensed, see LICENSE.txt.
